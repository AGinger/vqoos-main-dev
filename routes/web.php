<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@home')->name('home');
Route::post('/get-filtered-program', 'FrontController@getFilteredProgram');
Route::get('phone/verify', 'PhoneVerificationController@show')->name('phoneverification.notice');
Route::post('phone/verify', 'PhoneVerificationController@verify')->name('phoneverification.verify');


Auth::routes();

Route::prefix('admin')->group(function() {
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::resource('programs', 'Admin\ProgramController');
    Route::resource('dishes', 'Admin\DishController');
    Route::resource('coupons', 'Admin\CouponController');
    Route::resource('orders', 'Admin\OrderController');
    Route::resource('users', 'Admin\UserController');
    Route::resource('slides', 'SliderController');
    Route::post('storeProgramDish', 'Admin\DishController@storeProgramDish');
    Route::post('unlinkProgramDish', 'Admin\DishController@unlinkProgramDish');
}) ;

Auth::routes();

Route::group(['middleware' => 'auth'], function()
{
    Route::post('new-order', 'LkController@newOrder')->middleware('verifiedphone');
    Route::get('order/{id?}', 'LkController@order')->middleware('verifiedphone');
    Route::get('profile', 'LkController@profile')->middleware('verifiedphone');
    Route::post('profile', 'LkController@profileUpdate')->middleware('verifiedphone');
    Route::get('orders-history', 'LkController@ordersHistory')->middleware('verifiedphone');
    Route::post('get-promocode', 'LkController@getPromocode')->middleware('verifiedphone');
    Route::post('update-order-data', 'LkController@updateOrderData')->middleware('verifiedphone');
    Route::post('get-coords', 'LkController@getGeoCoords');
});
