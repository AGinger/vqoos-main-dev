<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->insert([
            'name' => 'Новый'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Подтвержден'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Не подтвержден'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Доставка'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Доставлен'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Закрыт'
        ]);
    }
}
