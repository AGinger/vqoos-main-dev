<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('program_id');
            $table->bigInteger('user_id');
            $table->date('delivery_date');
            $table->enum('delivery_period',['9.00 - 14.00','18.00 - 20.00']);
            $table->integer('price');
            $table->integer('delivery_price');
            $table->string('promocode')->nullable();
            $table->text('address');
            $table->string('note', 250)->nullable();
            $table->string('delivery_note', 255)->nullable();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
