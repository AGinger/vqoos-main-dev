<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("slug");
            $table->text("value");
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table("configs")->insert([
            "name"=>"Адрес",
            "slug"=>"address",
            "value"=>"Не установлено"
        ]);

        \Illuminate\Support\Facades\DB::table("configs")->insert([
            "name"=>"Телефон",
            "slug"=>"phone",
            "value"=>"Не установлено"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
