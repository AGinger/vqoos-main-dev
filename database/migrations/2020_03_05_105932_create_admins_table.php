<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("email")->unique();
            $table->string("password");
            $table->enum("role",["admin","operator"])->default("operator");
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table("admins")->insert([
            "id"=>1,
            "name"=>"Администратор",
            "email"=>"admin@vqoos.ru",
            "password"=>\Illuminate\Support\Facades\Hash::make("VqoosAdminPassword"),
            "role"=>"admin"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
