<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('name');
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->string('name')->nullable();
            $table->string('sms_code');
        });
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('email');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string("email")->unique()->nullable();

        });
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('password');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
