<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table)
        {
            $table->dropColumn('delivery_period');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->string("delivery_period")->nullable();
            $table->text("address")->nullable()->change();
            $table->date("delivery_date")->nullable()->change();
            $table->integer("price")->default(0)->change();
            $table->integer("delivery_price")->default(0)->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
