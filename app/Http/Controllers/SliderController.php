<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Image;

class SliderController extends Controller
{

    private $directoryName = 'slides';

    private $header = 'Слайдер';

    private $paramsForValidation = array(
        'alt'=>'required|max:255',
        'src'=>'required');

    private $fields = array(

        ['name'=>'Слайд', 'column'=>'src',  'type'=>'image'],
        ['name'=>'Слоган слайда', 'column'=>'alt',  'type'=>'text'],
        ['name'=>'Опубликовать', 'column'=>'visible',  'type'=>'checkbox']
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Slider::select()->get();
        if(!is_null($data->first())){
            $keys = array_keys($data->first()->toArray());
        }
        else $keys=array();
        return view('admin.directory_slides')->with(['items'=>$data,'header'=>$this->header,'keys'=>$keys,
            'directory'=>$this->directoryName,'fields'=>$this->fields]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = 'Добавление новой записи';
        return view('admin.directory_create')->with(['header'=>$header,'directory'=>$this->directoryName,'fields'=>$this->fields]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,$this->paramsForValidation);
        $item = new Slider();

        if($request->hasFile('src')) {

            $image = $request->file('src');
            $imageName = $image->getClientOriginalName();
            $filename = time().rand(111,999).$imageName;
            $path = public_path().'/uploads/slides/';
            $imageUrl = $path.$filename;
            Image::make($image)->fit(612,480)->save($imageUrl);
            $item->src = $filename;
        }
        $item->alt = $request->alt;
        $item->visible = $request->visible;
        $item->save();

        return redirect(route($this->directoryName.'.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Slider::find($id);

        $header = 'Изменение записи #'.$id;
        return view('admin.directory_edit')->with(['header'=>$header, 'item'=>$item, 'directory'=>$this->directoryName,
            'fields'=>$this->fields]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request);
        $item = Slider::find($id);
        if($request->hasFile('src')) {

            $image = $request->file('src');
            $imageName = $image->getClientOriginalName();
            $fileName = $item->id.time().rand(111,999).$imageName;

            $directory = public_path('/uploads/slides/');
            $imageUrl = $directory.$fileName;
            Image::make($image)->fit(612,480)->save($imageUrl);
            $item->src = $fileName;
        }
        $item->alt = $request->alt;
        if ($request->visible!=null)
        $item->visible = $request->visible;
        else $item->visible = 0;
        $item->save();

        return redirect(route($this->directoryName.'.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Slider::find($id);

        if ($item->delete()){
            return redirect(route($this->directoryName.'.index'));
        } else {
            return "fail";
        }
    }
}
