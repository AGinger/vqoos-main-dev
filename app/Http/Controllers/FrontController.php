<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\ProgramDish;
use App\Models\Slider;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function home()
    {
        $slides = Slider::where('visible',1)->get();
        $program = Program::with('dishes' )
            ->first();
        $selectedProgram = $program->evd;
        $selectedDuration = $program->duration;
        if ($program)
            $dishes = ProgramDish::with('dish')->where('program_id', $program->id)
                ->where('dish_id', '!=', 0)
                ->where('prg_day', 1)->get();

        return view('home')->with(compact('program', 'all_programs', 'dishes', 'selectedDuration', 'selectedProgram', 'slides'));
    }

    public function getFilteredProgram(Request $request)
    {
        $item = Program::with('dishes')->where('evd', $request->selectedEvd)->where('duration', $request->selectedDuration)->first();
        if ($item){
            $dishes = ProgramDish::with('dish', 'program')
                ->where('dish_id', '!=', 0)->where('program_id', $item->id)->get();

            return ['result'=>'success', 'item'=>$item, 'dishes'=>$dishes];
        }

        else return ['result'=>'fail'];

    }
}
