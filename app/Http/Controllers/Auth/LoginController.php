<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/phone/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showLoginForm() {
        return view('auth.user_login');
    }

    public function login(Request $request)
    {

        // Get user record
        $user = User::where('phone', $request->get('phone'))->first();
        if (!$user){
            return back()->with('status', 'Номер не найден');
        }

        $user->sms_code = mt_rand(1000, 9999);
        $user->phone_verified_at = null;
        $user->save();
        // Set Auth Details
        \Auth::login($user);

        // Redirect home page
        return redirect()->route('phoneverification.verify');
    }
}
