<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\OrderStatusHistory;
use App\Models\Program;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LkController extends Controller
{
    public function newOrder(Request $request)
    {
        $selectedProgram = Program::find($request->program_id);
        $order = new Order();
        $order->program_id = $request->program_id;
        $order->user_id = Auth::id();
        $order->name = mt_rand(1000, 9999);
        $order->price = $selectedProgram->price;
        if ($selectedProgram->duration % 2 == 0)
            $order->delivery_count = intdiv($selectedProgram->duration, 2);
        else
        $order->delivery_count = intdiv($selectedProgram->duration, 2) + 1;
        if($order->save()){
            $order_status = OrderStatusHistory::create([
                'status_id'=>3,
                'order_id'=>$order->id
            ]);
        }

        return redirect(url('order', ['id'=>$order->id]));
    }

    public function order($id=null)
    {
        if(!empty($id))
            $order = Order::find($id);
        else
            $order = Order::where('user_id', Auth::id())->orderBy('created_at', 'desc')->first();
        return view('lk.checkout')->with(compact('order'));
    }


    public function profile()
    {
        return view('lk.profile');
    }

    public function profileUpdate(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => ['required', 'regex:/[0-9]{10}/'],
        ]);
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->birthday))
        $user->birthday = Carbon::createFromFormat('m/d/Y', $request->birthday)->format('Y-m-d');
        $user->gender = $request->gender;
        if($request->has('phone_changed')){
            $user->phone = $request->phone;
            $user->phone_verified_at = null;
        }
        $user->save();

        return redirect()->back()->with('status', 'Данные успешно сохранены');
    }

    public function ordersHistory()
    {
        $orders = Order::with('program')->where('user_id', Auth::id())->get();
        return view('lk.orders-history')->with(compact('orders'));
    }

    public function getPromocode(Request $request)
    {
        $promocode = Coupon::where('promocode', $request->promocode)->first();
        if ($promocode)
            return ['status'=>'success', 'discount'=>$promocode->discount_amount];
        else return ['status'=>'fail'];
    }

    public function updateOrderData(Request $request)
    {
        $order = Order::find($request->order_id);
        $order->promocode = $request->promocode;
        $order->delivery_price = $request->delivery_price;
        $order->delivery_date = Carbon::parse( $request->delivery_date)->format('Y-m-d');
        $order->delivery_period = $request->delivery_period;
        $order->address = $request->address;
        $order->note = $request->note;
        $order->payment_type = $request->payment_type;
        $order->price = $request->price;
        $order->save();
        return redirect(url('orders-history'));
    }

    public function getGeoCoords()
    {
        $path = public_path() . "/data.geojson"; // ie: /var/www/laravel/app/storage/json/filename.json

        $json = json_decode($path, true);
        return $json;
    }
}
