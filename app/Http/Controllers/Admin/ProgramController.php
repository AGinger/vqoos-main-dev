<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\Program;
use App\Models\ProgramDish;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Program::all();
        return view('admin.programs.index')->with(compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.programs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required|max:255',
            'description'=>'required|max:500',
            'evd' => "required|in:750,1000,1500,2000,2500,3500",
            'duration' => 'required',
            'mpd' => 'required|integer|between:2,8',
            'price'=>'integer'
        ];

        $customMessages = [
            'name.max' => 'Поле название должно быть не больше 255 символов',
            'name.required' => 'Поле название не заполнено',
            'description.max' => 'Поле описание должно быть не больше 500 символов',
            'description.required' => 'Поле описание не заполнено',
            'mpd.between' => 'Число приемов пищи должно быть от 2 до 8',
            'mpd.integer' => 'Поле число приемов пищи должно быть числовым',
            'price.integer' => 'Поле цена должно быть числовым',
            'mpd.required' => 'Поле число приемов пищи не заполнено',
            'duration.required' => 'Выберите длительность программы'
        ];
        $request->validate($rules, $customMessages);
        $item = new Program();
        $item->name = $request->name;
        $item->evd = $request->evd;
        $item->duration = $request->duration;
        $item->description = $request->description;
        $item->mpd = $request->mpd;
        $item->price = $request->price;
        if($item->save())
        {
            for ($i=1; $i<=$item->duration; $i++){
                for ($j=1; $j<=$item->mpd; $j++){
                    $program_dish = new ProgramDish();
                    $program_dish->program_id = $item->id;
                    $program_dish->dish_id = 0;
                    $program_dish->prg_day = $i;
                    $program_dish->prg_day_meal = $j;
                    $program_dish->save();
                }
            }
        }

        return redirect(route('programs.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Program::find($id);
        $dishes = Dish::all();
        $program_dishes = ProgramDish::with('dish')->orderBy('prg_day')->where('program_id', $item->id)->get()->groupBy(function($item) {
            return $item->prg_day;
        });
        return view('admin.programs.edit')->with(['item'=>$item, 'program_dishes'=>$program_dishes, 'dishes'=>$dishes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'=>'max:255',
            'description'=>'required|max:500',
            'price'=>'integer'
        ];

        $customMessages = [
            'name.max' => 'Поле название должно быть не больше 255 символов',
            'price.integer' => 'Поле цена должно быть числовым',
            'description.max' => 'Поле описание должно быть не больше 500 символов',
        ];
        $request->validate($rules, $customMessages);
        $item = Program::find($id);
        $item->name = $request->name;
        $item->evd = $request->evd;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->save();

        return redirect(route('programs.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Program::find($id)->delete();
        ProgramDish::where('program_id', $id)->delete();
        return redirect()->route('programs.index')
            ->with('success','Программа успешно удалена');
    }
}
