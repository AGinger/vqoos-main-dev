<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\Program;
use App\Models\ProgramDish;
use Illuminate\Http\Request;
use Image;

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Dish::all();
        return view('admin.dishes.index')->with(compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dishes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'max:255',
            'carbohydrates'=>'integer', 'fats'=>'integer', 'proteins'=>'integer', 'calories'=>'integer',
            'photo' => 'image|required|mimes:jpeg,png,jpg',
            'description'=>'required|max:300'

        ],
            ['photo.required'=>'Загрузите иллюстрацию',
                'description.required'=>'Заполните поле описание',
                'description.max'=>'Поле описание должно содержать не больше 300 символов'
                ]);
        $dish = new Dish();
        $dish->name = $request->name;
        $dish->description = $request->description;
        $dish->calories = $request->calories;
        if (!empty($request->fats))
        $dish->fats = $request->fats;
        if (!empty($request->carbohydrates))
        $dish->carbohydrates = $request->carbohydrates;
        if (!empty($request->proteins))
        $dish->proteins = $request->proteins;
        $originalImage= $request->file('photo');
        $thumbnailImage = Image::make($originalImage);
        $originalPath= public_path().'/uploads/dishes/';
        $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
        $dish->photo=time().$originalImage->getClientOriginalName();
        $dish->save();
        return redirect()->route('dishes.edit', ['dish'=>$dish->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Dish::with('programs')->find($id);

        return view('admin.dishes.view')->with(['item'=>$item]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Dish::with('programs')->find($id);
        $free_program_dishes = ProgramDish::with('program')->where('dish_id', 0)->get();
        return view('admin.dishes.edit')->with(['item'=>$item, 'free_program_dishes'=>$free_program_dishes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'=>'max:255',
            'carbohydrates'=>'integer', 'fats'=>'integer', 'proteins'=>'integer', 'calories'=>'integer',
            'photo' => 'image|required|mimes:jpeg,png,jpg',
            'description'=>'required|max:300'

        ],
            ['photo.required'=>'Загрузите иллюстрацию',
                'description.required'=>'Заполните поле описание',
                'description.max'=>'Поле описание должно содержать не больше 300 символов'
            ]);
        $dish = Dish::find($id);
        $dish->name = $request->name;
        $dish->description = $request->description;
        $dish->calories = $request->calories;
        if (!empty($request->fats))
            $dish->fats = $request->fats;
        if (!empty($request->carbohydrates))
            $dish->carbohydrates = $request->carbohydrates;
        if (!empty($request->proteins))
            $dish->proteins = $request->proteins;
        if ($request->hasFile('photo')){
            $originalImage= $request->file('photo');
            $thumbnailImage = Image::make($originalImage);
            $originalPath= public_path().'/uploads/dishes/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
            $dish->photo=time().$originalImage->getClientOriginalName();
        }

        $dish->save();
        return redirect('admin/dishes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dish::find($id)->delete();
        ProgramDish::where('dish_id', $id)->delete();
        return redirect()->route('dishes.index')
            ->with('success','Блюдо успешно удалено');
    }

    public function storeProgramDish(Request $request)
    {

//        $item->programs()->syncWithoutDetaching([$item->id => ['dish_id' => $request->item_id]]);

        $program_dish = ProgramDish::find($request->selectedProgramId);
        $program_dish->dish_id = $request->item_id;
        $program_dish->save();

        $item = Dish::find($request->item_id);
        $program = $item->programs()->wherePivot('id', $program_dish->id)->first();
        $free_program_dishes = ProgramDish::with('program')->where('dish_id', 0)->get();
            return ['result'=>'success', 'program'=>$program, 'free_program_dishes'=>$free_program_dishes];

    }

    public function unlinkProgramDish(Request $request)
    {
        $program_dish = ProgramDish::find($request->item_id);
        $program_dish->dish_id = 0;
        $program_dish->load('dish', 'program');
        if($program_dish->save()){
            $free_program_dishes = ProgramDish::with('program')->where('dish_id', 0)->get();
            return ['result'=>'success', 'program_dish'=>$program_dish, 'free_program_dishes'=>$free_program_dishes];
        }

    }
}
