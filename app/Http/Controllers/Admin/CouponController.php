<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Coupon::all();

        return view('admin.coupons.index')->with(compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'promocode' => 'required|unique:coupons|max:255',
            'expiration_date' => 'required',
            'discount_amount' => 'required|integer|between:1,100',
        ]);
        $item = new Coupon();
        $item->promocode = $request->promocode;
        $item->expiration_date = Carbon::createFromFormat('Y-m-d', $request->expiration_date)->format('Y-m-d');
        $item->discount_amount = $request->discount_amount;
        $item->save();

        return redirect('admin/coupons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Coupon::find($id);
        return view('admin.coupons.edit')->with(compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'promocode' => 'required|unique:coupons|max:255',
            'expiration_date' => 'required',
            'discount_amount' => 'required|integer|between:1,100',
        ]);
        $item = Coupon::find($id);
        $item->promocode = $request->promocode;
        $item->expiration_date = Carbon::createFromFormat('Y-m-d', $request->expiration_date)->format('Y-m-d');
        $item->discount_amount = $request->discount_amount;
        $item->save();

        return redirect('admin/coupons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::find($id)->delete();
        return redirect()->route('coupons.index')
            ->with('success','Купон удален');
    }
}
