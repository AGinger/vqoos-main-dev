<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramDish extends Model
{
    public function dish()
    {
        return $this->belongsTo('App\Models\Dish');
    }

    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }
}
