<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{

    public function dishes()
    {
        return $this->belongsToMany('App\Models\Dish', 'program_dishes')->withPivot('prg_day', 'prg_day_meal', 'id');
    }
}
