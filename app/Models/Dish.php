<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    public function programs()
    {
        return $this->belongsToMany('App\Models\Program', 'program_dishes')->withPivot('id','prg_day','prg_day_meal');
    }
}
