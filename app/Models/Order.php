<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function program()
    {
        return $this->belongsTo('App\Models\Program');
    }

    public function statuses()
    {
        return $this->hasMany('App\Models\OrderStatusHistory', 'order_id', 'id')->orderByDesc('created_at');
    }

    public function coupon()
    {
        return $this->hasOne('App\Models\Coupon', 'promocode', 'promocode');
    }


}
