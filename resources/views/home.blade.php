@extends('layouts.main')

@section('content')
    <div class="carousel">
        <div class="main-carousel">
            @foreach($slides as $slide)
            <div class="carousel-cell">
                <div class="carousel__overlay">

                    <img class="carousel__img" src="{{asset('uploads/slides').'/'.$slide->src}}" alt="Alt Text!">

                    <div class="carousel__text">
                        <div class="carousel__text-inner">{{$slide->alt}}</div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>


    <form class="nutrition" method="post" action="{{url('new-order')}}">
        <h2>ПРОГРАММЫ ПИТАНИЯ</h2>
        @csrf
        <input type="hidden" name="program_id" :value="selectedProgramData.id">
        <div class="custom-select">
            <select @change="getFilteredProgram" name="selectedProgram" v-model="selectedProgram">
                <option selected value="750">750 Ккал</option>
                <option value="1000">1000 Ккал</option>
                <option value="1500">1500 Ккал</option>
                <option value="2000">2000 Ккал</option>
                <option value="2500">2500 Ккал</option>
                <option value="3500">3500 Ккал</option>
            </select>
            <p>Энергетическая ценность</p>
        </div>
        <div class="custom-radio nutrition__value">
            <div>
                <input @change="getFilteredProgram" v-model="selectedProgram" value="750" type="radio" name="nutrition_value" id="nutrition_750">
                <label for="nutrition_750">
                    750 Ккал
                </label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedProgram" value="1000" type="radio" name="nutrition_value" id="nutrition_1000">
                <label for="nutrition_1000">
                    1000 Ккал
                </label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedProgram" value="1500" type="radio" name="nutrition_value" id="nutrition_1500">
                <label for="nutrition_1500">
                    1500 Ккал
                </label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedProgram" value="2000" type="radio" name="nutrition_value" id="nutrition_2000">
                <label for="nutrition_2000">
                    2000 Ккал
                </label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedProgram" value="2500" type="radio" name="nutrition_value" id="nutrition_2500">
                <label for="nutrition_2500">
                    2500 Ккал
                </label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedProgram" value="3500" type="radio" name="nutrition_value" id="nutrition_3500">
                <label for="nutrition_3500">
                    3500 Ккал
                </label>
            </div>
        </div>

        <div class="custom-select nutrition__days">
            <select v-model="selectedDuration" name="">
                <option @change="getFilteredProgram"  value="2">2 дня</option>
                <option value="4">4 дня</option>
                <option value="5">5 дней</option>
                <option value="6">6 дней</option>
                <option value="12">12 дней</option>
                <option value="20">20 дней</option>
                <option value="24">24 дня</option>
                <option value="30">30 дней</option>
            </select>
            <p>Продолжительность</p>
        </div>
        <div class="custom-radio nutrition__days">
            <span>количество дней</span>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="2" type="radio" name="nutrition_day" id="day_2">
                <label for="day_2">2</label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="4" type="radio" name="nutrition_day" id="day_4">
                <label for="day_4">4</label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="5" type="radio" name="nutrition_day" id="day_5">
                <label for="day_5">5</label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="6" type="radio" name="nutrition_day" id="day_6">
                <label for="day_6">6</label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="12" type="radio" name="nutrition_day" id="day_12">
                <label for="day_12">12</label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="20" type="radio" name="nutrition_day" id="day_20">
                <label for="day_20">20</label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="24" type="radio" name="nutrition_day" id="day_24">
                <label for="day_24">24</label>
            </div>
            <div>
                <input @change="getFilteredProgram"  v-model="selectedDuration" value="30" type="radio" name="nutrition_day" id="day_30">
                <label for="day_30">30</label>
            </div>
        </div>
        <template v-if="selectedProgramData.dishes">
        <div  >
        <h3>@{{ selectedProgramData.name }}</h3>
        <h4>Дневная норма энергии – @{{ selectedProgramData.evd }} Ккал, приёмов пищи в день – @{{ selectedProgramData.mpd }} раз(а), длина программы – @{{ selectedProgramData.duration }} дн., всего @{{ selectedProgramData.dishes.length }}
            блюд.</h4>
        <p v-html="selectedProgramData.description"></p>
        <p class="price">Стоимость <strong>@{{ selectedProgramData.price }}</strong> ₽, @{{ selectedProgramData.price/selectedProgramData.duration }} ₽/день. </p>
        <button class="submit">ЗАКАЗАТЬ</button>
        </div>
        <p class="nutrition__info">Примерное меню по дням.</p>
        </template>
    </form>

    <div class="plan">
        <form class="nutrition" method="post" action="{{url('new-order')}}">
            @csrf
            <input type="hidden" name="program_id" :value="selectedProgramData.id">
            <div class="custom-select nutrition__days">
                <select @change="getFilteredProgram" v-model="selectedDay" name="">
                    <option  value="1">Понедельник</option>
                    <option value="2">Вторник</option>
                    <option value="3">Среда</option>
                    <option value="4">Четверг</option>
                    <option value="5">Пятница</option>
                    <option value="6">Суббота</option>
                    <option value="7">Воскресенье</option>
                </select>
                <p>Рацион дня</p>
            </div>
            <div class="custom-radio">
                <div>
                    <input @change="getFilteredProgram"  v-model="selectedDay" value="1"  type="radio" name="day_of_the_week" id="monday">
                    <label for="monday">
                        Понедельник
                    </label>
                </div>
                <div>
                    <input @change="getFilteredProgram"  v-model="selectedDay" value="2" type="radio" name="day_of_the_week" id="tuesday">
                    <label for="tuesday">
                        Вторник
                    </label>
                </div>
                <div>
                    <input @change="getFilteredProgram"  v-model="selectedDay" value="3" type="radio" name="day_of_the_week" id="wednesday">
                    <label for="wednesday">
                        Среда
                    </label>
                </div>
                <div>
                    <input @change="getFilteredProgram"  v-model="selectedDay" value="4" type="radio" name="day_of_the_week" id="thursday">
                    <label for="thursday">
                        Четверг
                    </label>
                </div>
                <div>
                    <input @change="getFilteredProgram"  v-model="selectedDay" value="5" type="radio" name="day_of_the_week" id="friday">
                    <label for="friday">
                        Пятница
                    </label>
                </div>
                <div>
                    <input @change="getFilteredProgram"  v-model="selectedDay" value="6" type="radio" name="day_of_the_week" id="saturday">
                    <label for="saturday">
                        Суббота
                    </label>
                </div>
                <div>
                    <input @change="getFilteredProgram"  v-model="selectedDay" value="7" type="radio" name="day_of_the_week" id="sunday">
                    <label for="sunday">
                        Воскресенье
                    </label>
                </div>
            </div>

            <template v-if="selectedProgramData.dishes||filteredDishes.length>0">
            <div class="foods">
                <div v-if="filteredDishes.length" v-for="dish in filteredDishes" class="food">

                    <div class="food__number">ПРИЁМ ПИЩИ №@{{ dish.prg_day_meal }}</div>


                    <img :src="'{{asset('uploads/dishes').'/'}}'+dish.dish.photo" alt="Alt Text!">


                    <div class="food__name">@{{ dish.dish.name }}</div>
                    <div class="food__components">
                        <span v-html="dish.dish.description"></span>
                    </div>
                </div>

            </div>
            <p class="price">Стоимость <strong>@{{ selectedProgramData.price }}</strong> ₽, @{{ selectedProgramData.price/selectedProgramData.duration }} ₽/день. </p>
            <button class="submit">ЗАКАЗАТЬ</button>
            </template>
        </form>
    </div>

@stop

@section('js')

    <script>
        var app = new Vue({
            el: '#main-wrapper',
            data() {
                return {
                    selectedProgram:'{!! $selectedProgram !!}',
                    selectedDuration:{!! $selectedDuration !!},
                    selectedProgramData:{!! $program?$program->toJson():'' !!},
                    selectedDay:1,
                    selectedDishes:{!! $dishes?$dishes->toJson():'' !!},
                }
            },

            methods: {
                getFilteredProgram()
                {
                    var self = this;
                    var selectedEvd = this.selectedProgram,
                        selectedDuration = this.selectedDuration;
                    axios.post('/get-filtered-program',{
                        selectedDuration:selectedDuration,selectedEvd:selectedEvd
                    })
                        .then(function (response) {
                            if(response.data.result === 'success'){
                                // console.log(response.data);
                                self.selectedDishes = response.data.dishes;
                                self.selectedProgramData = response.data.item;
                            }
                            else {self.selectedProgramData = [];
                                self.selectedDishes = []}
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
            },

            computed: {

                filteredDishes()
                {
                    const vm = this;
                    if (!this.selectedDay.length)
                        return this.selectedDishes;

                    posts = this.selectedDishes.filter(
                        function(dish){
                            // self.selectedProgramData = program;
                            return dish.prg_day == vm.selectedDay;

                        }

                    )

                    return posts;

                },


            }
        })
    </script>

    @stop