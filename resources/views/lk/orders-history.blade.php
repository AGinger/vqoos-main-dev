@extends('layouts.lk')

@section('content')
    <div class="content">
        <div class="content__header">
            <h1>ИСТОРИЯ ЗАКАЗОВ</h1>
            <p>{{\Illuminate\Support\Facades\Auth::user()->name}} {{\Illuminate\Support\Facades\Auth::user()->phone}}</p>
            <a class="logout" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="icon icon-exit"></i>
                выйти
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
        <div class="content__body">
            <div class="history">
                <div class="order__grid order__header">
                    <div class="order__id">
                        <i class="icon icon-plan"></i>ID
                    </div>
                    <div class="order__date">
                        <i class="icon icon-today"></i>доставки
                    </div>
                    <div class="order__adress">
                        <i class="icon icon-adress"></i>Адрес
                    </div>
                    <div class="order__price">
                        ₽ <span>ЦЕНА</span>
                    </div>
                    <div class="order__status">
                        Статус
                    </div>
                </div>
                @foreach($orders as $order)
                <div class="order__grid">
                    <div class="order__id">
                        {{$order->name}}
                    </div>
                    <div class="order__date">
                        {{$order->delivery_date}}
                    </div>
                    <div class="order__adress">
                        {{$order->address}}
                    </div>
                    <div class="order__price">
                        {{$order->price}}
                    </div>
                    <div class="order__status">
                        Доставка
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
    @stop