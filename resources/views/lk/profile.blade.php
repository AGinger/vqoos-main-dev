@extends('layouts.lk')

@section('content')
    <div class="content">
        <div class="content__header">
            <h1>ЛИЧНЫЕ ДАННЫЕ</h1>
            <p>{{\Illuminate\Support\Facades\Auth::user()->name}} {{\Illuminate\Support\Facades\Auth::user()->phone}}</p>
            <a class="logout" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="icon icon-exit"></i>
                выйти
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
        <div class="content__body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form class="personal-data" method="post" action="{{url('profile')}}">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="field name">
                    <i class="icon icon-user"></i>
                    <input type="text" name="name" placeholder="Ваше имя" value="{{Auth::user()->name}}" autocomplete="off">
                    <div class="field-info">
                        Имя, обязательно
                    </div>
                    <span class="field__edit-btn">
                            <i class="icon icon-edit"></i>
                        </span>
                </div>
                <div class="field phone">
                    <i class="icon icon-phone"></i>
                    <input name="phone" type="text" placeholder="Ваш телефон" value="{{Auth::user()->phone}}" autocomplete="off">
                    <div class="field-info">
                        Телефон, обязательно
                    </div>
                    <span class="field__edit-btn">
                            <i class="icon icon-edit"></i>
                        </span>
                </div>
                <div class="field birthday">
                    <i class="icon icon-calendar"></i>
                    <input name="birthday" type="date" placeholder="День рождения" value="{{Auth::user()->birthday}}" autocomplete="off">
                    <div class="field-info">
                        День рождения, не обязательно
                    </div>
                    <span class="field__edit-btn">
                            <i class="icon icon-edit"></i>
                        </span>
                </div>
                <div class="field gender">
                    <i class="icon icon-peoples"></i>
                    <select name="gender">
                        <option value="male">Мужской</option>
                        <option value="female">Женский</option>
                    </select>
                    <div class="field-info">
                        Пол, не обязательно
                    </div>
                    <span class="field__edit-btn">
                            <i class="icon icon-edit"></i>
                        </span>
                </div>
                <div class="field mail">
                    <i class="icon icon-calendar"></i>
                    <input name="email" type="text" placeholder="Почта" value="{{Auth::user()->email}}" autocomplete="off">
                    <div class="field-info">
                        Эл. почта, не обязательно
                    </div>
                    <span class="field__edit-btn">
                            <i class="icon icon-edit"></i>
                        </span>
                </div>
                <button type="submit">ПОДТВЕРДИТЬ</button>
            </form>
        </div>
    </div>
    @stop