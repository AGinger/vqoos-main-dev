@extends('layouts.lk')

@section('content')

<div class="content" id="app">
    <div class="content__header">
        <h1>ГЛАВНЫЙ ПУЛЬТ</h1>
        <p>{{\Illuminate\Support\Facades\Auth::user()->name}} {{\Illuminate\Support\Facades\Auth::user()->phone}}</p>
        <a class="logout" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="icon icon-exit"></i>
            выйти
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST"  style="display:none;">
        {{ csrf_field() }}
        </form>


    </div>
    <div class="content__body order">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ url('update-order-data') }}" method="POST" >
            {{ csrf_field() }}
            <input type="hidden" name="order_id" value="{{$order->id}}">
        <div class="order__info">
                    <span class="order__data">
                        <i class="icon icon-plan"></i>
                        ЗАКАЗ {{$order->name}} от {{\Carbon\Carbon::parse($order->created_at)->format('d.m.Y')}}
                    </span>
            <div class="order__badges">
                <span class="order__badge order__badge--green">Не подтвержден</span>
                <span class="order__badge order__badge--orange">не оплачен</span>
            </div>
            <button class="order__delete">
                <i class="icon icon-close"></i>
                Удалить заказ
            </button>
        </div>

        <div class="row ">
            <div>
                <div class="field promo">
                    <i class="icon icon-100"></i>
                    <input id="promocode" name="promocode" type="text" placeholder="Промокод">
                    <div id="promocodeResult" class="field-info">Введите если есть</div>
                </div>
                <div class="field discount">
                    <span class="icon">%</span>
                    <input value="0" id="promocodeValue" name="promocodeValue" disabled type="text" autocomplete="off">
                    <div class="field-info">Размер скидки</div>
                </div>
            </div>

            <div class="field delivery-date">
                <i class="icon icon-today"></i>
                <input required type="date" placeholder="Дата доставки" name="delivery_date" value="{{$order->delivery_date}}">
                <div class="field-info">Обязательно</div>
            </div>

            <div class="field delivery-time">
                <i class="icon icon-clock"></i>
                <select name="delivery_period" id="">
                    <option @if($order->delivery_period=='9.00-14.00') selected @endif value="9.00-14.00">9.00 - 14.00</option>
                    <option @if($order->delivery_period=='10.00-14.00') selected @endif value="10.00-14.00">10.00 - 14.00</option>
                    <option @if($order->delivery_period=='10.00-14.00') selected @endif value="11.00-14.00">11.00 - 14.00</option>
                </select>
                <div class="field-info">
                    Время доставки
                </div>
            </div>
        </div>
        <div class="row">
            <div class="field adress">
                <i class="icon icon-adress"></i>
                <input required type="text" placeholder="Адрес доставки" id="address" name="address" value="{{$order->address}}">
                <input type="hidden" id="addressCoords" name="coords">
                <div id="address-error" class="field-info">Расчитывается в зависимости от зон доставки</div>
            </div>
            <div class="field delivery-price">
                <span class="icon">₽</span>
                <input id="deliveryprice" name="delivery_price" readonly type="text" value="{{$order->delivery_price}}">
                <input type="hidden" id="delivery_single" value="0">
                <div class="field-info">Стоимость доставки</div>
            </div>
        </div>
        <div class="row">
            <div class="field note">
                <i class="icon icon-file"></i>
                <textarea name="note" maxlength="200" placeholder="Примечание" rows="2">{{$order->note}}</textarea>
                <div class="field-info">Не обязательно, 200 символов макс.</div>
            </div>
        </div>
        <div class="all-programms">
            <div class="programm">
                <div class="programm__info">
                    <h2>{{$order->program->name}}</h2>
                    <p>{!! $order->program->description !!}</p>
                </div>
                <button class="order__delete"></button>
                <div class="field programm__price">
                    <span class="icon">₽</span>
                    <input disabled type="text" placeholder="{{$order->program->price}}">
                    <div class="field-info">Стоимость программы</div>
                </div>
            </div>
        </div>
        <div class="row total">
            <div class="field">
                <span class="icon">₽</span>
                <input id="price" name="price" readonly type="text" value="{{$order->price}}">
                <div class="field-info">Итоговая стоимость</div>
            </div>
        </div>
        <div class="row total">
            <div class="pay-method">
                <div>
                    <input type="radio" name="payment_type" id="pay-method-1" value="prepay" @if($order->payment_type=='prepay') checked @endif>
                    <label for="pay-method-1">Безнал сейчас</label>
                </div>
                <div>
                    <input type="radio" name="payment_type" id="pay-method-2" value="postpay" @if($order->payment_type=='postpay') checked @endif>
                    <label for="pay-method-2">Курьеру безналом</label>
                </div>
                <div>
                    <input type="radio" name="payment_type" id="pay-method-3" value="cash" @if($order->payment_type=='cash') checked @endif>
                    <label for="pay-method-3">Курьеру налом</label>
                </div>
            </div>
            <button type="submit">
                Отправить
            </button><br />
        </div>
        </form>
    </div>
    <div id="map"></div>
</div>

    @stop

@section('js')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;coordorder=longlat&amp;apikey=9aedc04f-5a19-45f0-96ee-0d9cd060d47d" type="text/javascript"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://yandex.st/jquery/2.2.3/jquery.js"></script>
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });
        function update_amounts()
        {
            var
                orderPrice = '{{$order->price}}',
                promocodeValue=parseInt($('#promocodeValue').val()),
                deliveryCount = '{{$order->delivery_count}}',
                deliveryPrice =  $('#delivery_single').val();
            //calculate total + shipping and add VAT
            var shipping = parseInt(deliveryPrice)*parseInt(deliveryCount);
            var total = parseInt(orderPrice)+parseInt(shipping);
            var discount_value = ((total*promocodeValue)/100);
            //calculate grand total

            var sub_total = (parseInt(total)-parseInt(discount_value));
            console.log(sub_total);
            $('#deliveryprice').val(shipping);
            $('#price').val(sub_total);
        }
        $("#promocode").change(function(e){

            e.preventDefault();
            var promocode = $("input[name=promocode]").val();
            $.ajax({

                type:'POST',

                url:'/get-promocode',

                data:{promocode:promocode},

                success:function(response){

                    if(response.status === 'success'){
                        // console.log(response.data);
                        $("input[name=promocodeValue]").val(response.discount);
                        $("#promocodeResult").text('Введите если есть');
                        update_amounts();
                    }
                    else {
                        $("input[name=promocodeValue]").val(0);
                        $("#promocodeResult").text('Промокод не найден');
                        update_amounts();
                    }

                }

            });
        });


    </script>
    <script type="text/javascript" src="{{asset('js/checkout.js')}}"></script>
    @stop