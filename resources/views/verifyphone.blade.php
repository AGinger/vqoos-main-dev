<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="{{asset('icons/css/fontello.css')}}">
    <!--[if IE 7]><link rel="stylesheet" href="css/" + font.fontname + "-ie7.css"><![endif]-->
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('css/login-page.css')}}">
</head>

<body>
<div class="login-page sms-confirm">
    <form method="post" action="{{ route('phoneverification.verify') }}">
        @csrf
        <a href="{{url('/')}}" class="header__logo"></a>
        <a href="{{ url()->previous() }}" class="link"><i class="icon icon-prev"></i> назад</a>
        <h2>Введите код из SMS</h2>
        <div class="custom-num-input">
            <input  id="phone-num" type="number" name="code" required>
            <span>{{$code}}</span>
            @if ($errors->has('code'))
                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('code') }}</strong>
                                                </span>
            @endif
        </div>
        <div class="input-message">Обязательно</div>

        <button type="submit">Продолжить</button>
        <p>Отправить повторно через <span>20</span> с</p>
        <a href="{{url('/login')}}" class="link"><i class="icon icon-prev"></i> изменить номер</a>
    </form>
</div>
</body>

</html>
