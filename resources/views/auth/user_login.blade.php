<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="{{asset('icons/css/fontello.css')}}">
    <!--[if IE 7]><link rel="stylesheet" href="css/" + font.fontname + "-ie7.css"><![endif]-->
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('css/login-page.css')}}">
</head>

<body>
<div class="login-page">
    <form method="post" action="{{url('/login')}}">
        {{ csrf_field() }}
        <a href="{{url('/')}}" class="header__logo"></a>
        <a href="{{ url()->previous() }}" class="link"><i class="icon icon-prev"></i> назад</a>
        <h2>Введите ваш телефон</h2>
        <div class="custom-num-input">
            <i class="icon icon-phone"></i>
            <!-- need to create input mask -->
            <span>8</span>
            <input type="number" required name="phone" id="phone-num" placeholder="9655555555" pattern="[0-9]{3}-[0-9]{7}">
            @if (session('status'))
                {{ session('status') }}
            @endif
        </div>
        <div class="input-message">Обязательно</div>

        <div class="recaptcha">
            место для рекаптчи
        </div>
        <button type="submit">Продолжить</button>
        <p>Нажимая кнопку «Продолжить», вы соглашаетесь с <a>условиями оферты</a> и даете согласие на <a>обработку персональных данных</a>.</p>
    </form>
</div>
</body>

</html>