@extends('adminlte::page')

<div class="panel panel-default box box-primary" id="app">
    @section('content_header')
        <h1>Редактировать купон</h1>
        <div class="panel-heading fa-pull-right">

        </div>
    @stop

    @section('content')

        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->

                    <div class="card card-primary card-tabs">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" action="{{url('admin/coupons/'.$item->id)}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="fa-pull-left" for="promocode">Промокод</label>
                                    <input value="{{$item->promocode}}" name="promocode" type="text" class="form-control" id="promocode" placeholder="До 10 символов, без пробелов">
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="discount_amount">Скидка в %</label>
                                    <input value="{{$item->discount_amount}}" name="discount_amount" type="text" class="form-control" id="discount_amount" placeholder="Значение скидки в %">
                                </div>
                                <div class="form-group">

                                    <label>Срок истечения:</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                          </span>
                                        </div>
                                        <input value="{{$item->expiration_date}}" type="text" name="expiration_date" class="form-control pull-right" id="datepicker" placeholder="Выберите дату">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>


    @stop


        @section('css')

            <link rel="stylesheet" href="{{asset('/vendor/air/css/datepicker.css')}}">
        @stop

        @section('js')

            <script src="{{asset('/vendor/air/js/datepicker.js')}}"></script>
            <script>

                $('#datepicker').datepicker({dateFormat: "yyyy-mm-dd"});

            </script>
@stop