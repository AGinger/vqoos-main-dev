@extends('adminlte::page')
<div class="panel panel-default box box-primary" id="app">
	@section('content_header')
		<h1>Новая запись</h1>

		<div class="panel-heading">

		</div>
	@stop
@section('content')

	<div class="panel panel-default box box-primary" id="app">
		<div class="panel-heading">
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			{{ Form::open(['url' => route($directory.'.store'), 'files' => true]) }}
				@foreach($fields as $item)
				@php $field_column = $item['column']; @endphp
				<div class="form-group @if($errors->get($field_column)) has-error @endif">
					<label for="{{$item['column']}}">{{$item['name']}}</label>
					@if($item['type']=='text')
					{{ Form::text($item['column'],false,['class'=>'form-control']) }}
					@elseif($item['type']=='textarea')
					{{ Form::textarea($item['column'],false,['class'=>'form-control','id'=>'editor']) }}
					@elseif($item['type']=='select')
					{{ Form::select('hashtags[]', $select->pluck('name', 'id'), false,['data-placeholder'=>'Выберите','class'=>'form-control select2','multiple'=>'multiple']) }}
					@elseif($item['type']=='select_simple')
						{{ Form::select($item['column'], $select->pluck('name', 'id'), false,['data-placeholder'=>'Выберите','class'=>'form-control']) }}
					@elseif($item['type']=='checkbox')
					{{ Form::checkbox($item['column'], 1, true) }}
					@elseif($item['type']=='image')
						<div class="form-group top">
							<input type="file" name="src"  id="add_photo" style="display: none">
							<img style="width: 350px; " src="" id="preview" alt="">
							<div class="btn-cont_">
								<label for="add_photo">
									<a class="btn btn-info upload-photo">
										Загрузить файл
									</a>
								</label>
							</div>
							<br>
						</div>
					@endif
				</div>
				<br>
				@endforeach
				{{ Form::submit('Создать',['class' => 'btn btn-success']) }}
					{{ csrf_field() }}
				{{ Form::close() }}
		</div>


	</div>

@endsection
@section('js')
	<script>
        $("input#add_photo").change(function (e) {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }

        });
	</script>
@endsection
</div>
