@extends('adminlte::page')
<div class="panel panel-default box box-primary" id="app">
	@section('content_header')
		<h1>Редактировать запись</h1>
		<div class="panel-heading">

		</div>
	@stop
@section('content')
	<div class="panel panel-default box box-primary">
		<div class="panel-heading">
			{{ Form::open(['url' => route($directory.'.update',['slide'=>$item->id]),'method' => 'PUT', 'files' => true])}}
				@foreach($fields as $field)
				@php $field_column = $field['column']; @endphp
				<div class="form-group @if($errors->get($field_column)) has-error @endif">
					<label for="{{$field_column}}">{{$field['name']}}</label>
					@if($field['type']=='text')
					{{ Form::text($field_column,$item->$field_column,['class'=>'form-control']) }}
					@elseif($field['type']=='textarea')
					{{ Form::textarea($field_column,$item->$field_column,['class'=>'form-control','id'=>'summernote']) }}
					@elseif($field['type']=='select')
					{{ Form::select('hashtags[]', $select->pluck('name', 'id'), array_values($item_tags),['data-placeholder'=>'Выберите','class'=>'form-control select2','multiple'=>'multiple']) }}
					@elseif($field['type']=='select_simple')
						{{ Form::select($field_column, $select->pluck('name', 'id'), $item->$field_column,['data-placeholder'=>'Выберите','class'=>'form-control']) }}
					@elseif($field['type']=='checkbox')
					{{ Form::checkbox('visible', 1, $item->$field_column) }}
					@elseif($field['type']=='image')
						<div class="form-group top">
							<input type="file" name="image"  id="add_photo" style="display: none">

								<img style="width: 350px; " src="{{asset('uploads/slides').'/'.$item->src}}" id="preview" alt="">

							<div class="btn-cont_">
								<label for="add_photo">
									<a class="btn btn-info upload-photo">
										Загрузить файл
									</a>
								</label>
							</div>
						</div>
					@endif
				</div>
				<br>
				@endforeach
				{{ Form::submit('Изменить',['class' => 'btn btn-success']) }}
				{{ csrf_field() }}
			{{ Form::close() }}
		</div>
	</div>

@endsection

@section('js')
	<script>
        $("input#add_photo").change(function (e) {
            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }

        });
	</script>
@endsection
</div>