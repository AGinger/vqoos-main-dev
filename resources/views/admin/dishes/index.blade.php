@extends('adminlte::page')

@section('title', 'Блюда')
<div class="panel panel-default box box-primary">
    @section('content_header')
        <h1>Блюда</h1>
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">

            </div>
            <ul class="nav navbar-nav">
                <li> <a href="{{url('/admin/dishes/create')}}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Добавить
                    </a></li>
            </ul>
        </nav>
    @stop

    @section('content')

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Название</th>
                <th>Описание</th>
                <th>Изображение</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{!! $item->description !!}</td>
                    <td><img style="width: 100px;" src="{{asset('uploads/dishes'.'/'.$item->photo)}}"/></td>
                    <td>
                        <form class="delete" action="{{ route('dishes.destroy',$item->id) }}" method="POST">

                            <a href="{{url('admin/dishes/'.$item->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>
                            <a href="{{url('admin/dishes/'.$item->id)}}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger btn-sm" onsubmit="confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @stop
</div>

@section('js')
    <script src="{{asset('/vendor/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        $(".delete").on("submit", function(){
            return confirm("Удалить блюдо?");
        });

    </script>
@stop