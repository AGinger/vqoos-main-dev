@extends('adminlte::page')

<div class="panel panel-default box box-primary" id="app">
    @section('content_header')
        <h1>Редактировать блюдо</h1>
        <div class="panel-heading fa-pull-right">

        </div>
    @stop

    @section('content')

        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->

                    <div class="card card-primary card-tabs">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" action="{{url('admin/dishes/'.$item->id)}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="fa-pull-left" for="name">Название</label>
                                    <input required value="{{$item->name}}" name="name" type="text" class="form-control" id="name" placeholder="Название">
                                </div>
                                <div class="form-group">
                                    <label class="" for="description">Описание</label>
                                    <textarea name="description" class="textarea" id="description" placeholder="Описание">{{$item->description}}</textarea>
                                </div>
                                <div class="form-group top">
                                    <input type="file" name="photo"  id="add_photo" style="display: none">
                                    <img style="width: 200px; " src="{{asset('uploads/dishes'.'/'.$item->photo)}}" id="preview" alt="">
                                    <div class="btn-cont_">
                                        <label for="add_photo">
                                            <a style="color: white" class="btn btn-info upload-photo">
                                                Загрузить иллюстрацию
                                            </a>
                                        </label>
                                    </div>
                                    <br>
                                </div>
                                <div class="row">
                                <div class="col-4 ">
                                <div class="form-group">
                                    <label class="fa-pull-left" for="calories">Энергетическая ценность в ккал</label>
                                    <input value="{{$item->calories}}" name="calories" type="text" class="form-control" id="calories" placeholder="Энергетическая ценность в ккал"/>
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="carbohydrates">Углеводы</label>
                                    <input  value="{{$item->carbohydrates}}" name="carbohydrates" type="text" class="form-control" id="carbohydrates" placeholder="Углеводы">
                                </div>
                                </div>
                                <div class="col-4">
                                <div class="form-group">
                                    <label class="fa-pull-left" for="proteins">Белки</label>
                                    <input value="{{$item->proteins}}" name="proteins" type="text" class="form-control" id="proteins" placeholder="Белки">
                                </div>

                                <div class="form-group">
                                    <label class="fa-pull-left" for="fats">Жиры</label>
                                    <input value="{{$item->fats}}" name="fats" type="text" class="form-control" id="fats" placeholder="Жиры">
                                </div>
                                </div></div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>

                        <h3>Привязки</h3>
                        <button class="btn-info" data-toggle="modal" data-target="#modal-lg">Добавить привязку</button>
                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                            <thead>
                            <tr role="row">
                                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1" >Программа</th>
                                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1">День</th>
                                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Прием</th>
                                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Действия</th></tr>
                            </thead>
                            <tbody>
                            <tr v-for="(program_dish, key) in item.programs" role="row" class="odd">
                                <td>@{{ program_dish.name }}</td>
                                <td>@{{ program_dish.pivot.prg_day }}</td>
                                <td>@{{ program_dish.pivot.prg_day_meal }}</td>
                                <td class="project-actions">
                                    <a :href="'{{url('/admin/programs')}}/'+program_dish.id+'/edit'" class="btn btn-info"><i class="fas fa-folder">                                 </i>
                                        Смотреть программу
                                    </a>
                                    <button @click="unlinkProgramDish(program_dish.pivot.id, key)" class="btn btn-danger">
                                        <i class="fas fa-trash">
                                        </i>
                                        Отвязать
                                    </button></td>
                            </tr></tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div id="modal-lg" class="modal fade" style="display: none;">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Добавить привязку</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Программа</label>
                            <select v-model="selectedProgramId" class="form-control">
                                <option value="">Выберите</option>
                                <option :value="program_dish.id" v-for="program_dish in free_program_dishes">
                                    @{{ program_dish.program.name }} - День @{{ program_dish.prg_day }} - Прием @{{ program_dish.prg_day_meal }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-primary" @click="storeProgramDish">Сохранить</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        </div>

    @stop

    @section('css')

        <link rel="stylesheet" href="{{asset('/vendor/summernote/summernote-bs4.css')}}">
    @stop

    @section('js')
        <script src="{{asset('/vendor/summernote/summernote-bs4.min.js')}}"></script>
        <script src="{{asset('/js/vue.min.js')}}"></script>
        <script src="{{asset('/js/axios.min.js')}}"></script>
        <script>

            var app = new Vue({
                el: '#app',
                data: {
                    item: {!!$item->toJson()!!},
                    free_program_dishes:{!! $free_program_dishes->toJson() !!},
                    selectedProgramId:'',
                },
                methods:{
                    storeProgramDish(){
                        var self = this;
                        var item_id = this.item.id,
                            selectedProgramId = this.selectedProgramId;
                        axios.post('/admin/storeProgramDish',{
                            item_id:item_id,selectedProgramId:selectedProgramId
                        })
                            .then(function (response) {
                                if(response.data.result === 'success'){
                                    self.item.programs.push(response.data.program);
                                    self.free_program_dishes = response.data.free_program_dishes;
                                    $('#modal-lg').modal('hide');
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    },

                    unlinkProgramDish(id, key) {
                        if (confirm("Удалить связь?")) {
                            var self = this;
                            axios.post('/admin/unlinkProgramDish', {
                                item_id: id
                            })
                                .then(function (response) {

                                    if (response.data.result === 'success') {
                                        self.item.programs.splice(key, 1);
                                        self.free_program_dishes = response.data.free_program_dishes;
                                    }
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        }
                    }
                }
            });
            $('#description').summernote();
            $("input#add_photo").change(function (e) {

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(this.files[0]);
                }

            });

        </script>
@stop