@extends('adminlte::page')

<div class="panel panel-default box box-primary" id="app">
    @section('content_header')
        <h1>Просмотр блюда</h1>
        <div class="panel-heading fa-pull-right">

        </div>
    @stop

    @section('content')

        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->

                    <div class="card">

                        <div class="card-header">
                            <h3 class="card-title">
                              <img src="{{asset('uploads/dishes').'/'.$item->photo}}">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-4">Название</dt>
                                <dd class="col-sm-8">{{$item->name}}</dd>
                                <dt class="col-sm-4">Описание</dt>
                                <dd class="col-sm-8">{!! $item->description !!}</dd>
                                <dt class="col-sm-4">Энергетическая ценность блюда</dt>
                                <dd class="col-sm-8">{{$item->calories}}</dd>
                                <dt class="col-sm-4">Белки</dt>
                                <dd class="col-sm-8">{{$item->proteins}}
                                </dd>
                                <dt class="col-sm-4">Жиры</dt>
                                <dd class="col-sm-8">{{$item->fats}}
                                </dd>
                                <dt class="col-sm-4">Углеводы</dt>
                                <dd class="col-sm-8">{{$item->carbohydrates}}
                                </dd>
                            </dl>
                        </div>
                        <!-- /.card-body -->

                    </div>
                </div>
                <div class="col-md-6">
                <h3>Привязки</h3>
                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                    <tr role="row">
                        <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1" >Программа</th>
                        <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1">День</th>
                        <th tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Прием</th>
                       </tr>
                    </thead>
                    <tbody>
                    @foreach($item->programs as $program_dish)
                    <tr role="row" class="odd">
                        <td>{{ $program_dish->name }}</td>
                        <td>{{ $program_dish->pivot->prg_day }}</td>
                        <td>{{ $program_dish->pivot->prg_day_meal }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
                </div>
            </div>

        </div>

    @stop

