@extends('adminlte::page')

@section('title', 'Блюда')
<div class="panel panel-default box box-primary">
    @section('content_header')
        <h1>Добавить блюдо</h1>
        <div class="panel-heading fa-pull-right">

        </div>
    @stop

    @section('content')

        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->

                    <div class="card card-primary card-tabs">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" action="{{url('admin/dishes')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="fa-pull-left" for="name">Название</label>
                                    <input name="name" type="text" class="form-control" id="name" placeholder="Название" required>
                                </div>
                                <div class="form-group">
                                    <label class="" for="description">Описание</label>
                                    <textarea name="description" class="textarea" id="description" placeholder="Описание"></textarea>
                                </div>
                                <div class="form-group top">
                                    <input type="file" name="photo"  id="add_photo" style="display: none">
                                    <img style="width: 350px; " src="" id="preview" alt="">
                                    <div class="btn-cont_">
                                        <label for="add_photo">
                                            <a style="color: white" class="btn btn-info upload-photo">
                                                Загрузить иллюстрацию
                                            </a>
                                        </label>
                                    </div>
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="calories">Энергетическая ценность в ккал</label>
                                    <input name="calories" type="text" class="form-control" id="calories" placeholder="Энергетическая ценность в ккал"/>
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="carbohydrates">Углеводы</label>
                                    <input  name="carbohydrates" type="text" class="form-control" id="carbohydrates" placeholder="Углеводы">
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="proteins">Белки</label>
                                    <input  name="proteins" type="text" class="form-control" id="proteins" placeholder="Белки">
                                </div>

                                <div class="form-group">
                                    <label class="fa-pull-left" for="fats">Жиры</label>
                                    <input  name="fats" type="text" class="form-control" id="fats" placeholder="Жиры">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

    @stop

    @section('css')

        <link rel="stylesheet" href="{{asset('/vendor/summernote/summernote-bs4.css')}}">
    @stop

    @section('js')
        <script src="{{asset('/vendor/summernote/summernote-bs4.min.js')}}"></script>
        <script>
            $('#description').summernote();
            $("input#add_photo").change(function (e) {

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(this.files[0]);
                }

            });

        </script>
@stop