@extends('adminlte::page')

<div class="panel panel-default box box-primary">
    @section('content_header')
        <h1>Пользователи</h1>
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">

            </div>
        </nav>
    @stop

    @section('content')

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Телефон</th>
                <th>Имя
                <th>Пол</th>
                <th>Почта</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{$item->phone}}</td>
                    <td>{!! $item->name !!}</td>
                    <td>{!! $item->gender !!}</td>
                    <td>{!! $item->email !!}</td>
                    <td>
                        <form class="delete" action="{{ route('users.destroy',$item->id) }}" method="POST">

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @stop
</div>

@section('js')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Удалить пользователя?");
        });

    </script>
@stop