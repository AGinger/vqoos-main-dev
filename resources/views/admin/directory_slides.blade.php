@extends('adminlte::page')
<div class="panel panel-default box box-primary" id="app">
    @section('content_header')
        <h1>Слайды</h1>

        <div class="panel-heading">

        </div>
    @stop
@section('content')
    <div class="panel panel-default box box-primary">
        <div class="panel-heading">
            <a href="{{route($directory.'.create')}}" class="btn btn-primary">
                <i class="fa fa-plus"></i> Добавить
            </a>


        </div>
        <table id="table_admin" class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Слайд</th>
                <th>Слоган слайда</th>
                <th>Опубликовать</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{$item->id}}</td>
                        <td><img style="width: 150px;" src="{{asset('uploads/slides/'.$item->src)}}" /></td>
                        <td>{{$item->alt}}</td>
                        <td>{{$item->visible}}</td>
                    <td class="text-right">
                        <a href="{{route($directory.'.edit',['slide'=>$item->id])}}" class="btn btn-xs btn-primary" title="" data-toggle="tooltip" data-original-title="Edit"><i class="fas fa-pencil-alt"></i></a>
                        <form action="{{route($directory.'.destroy',['slide'=>$item->id])}}" method="POST" style="display:inline-block;">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <button class="btn btn-xs btn-danger btn-delete" title="" data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
</div>