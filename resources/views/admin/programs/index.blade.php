@extends('adminlte::page')

<div class="panel panel-default box box-primary">
@section('content_header')
    <h1>Программы</h1>
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">

            </div>
            <ul class="nav navbar-nav">
                <li> <a href="{{url('/admin/programs/create')}}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Добавить
                    </a></li>
            </ul>
        </nav>
        {{--<div class="panel-heading fa-pull-right">--}}
            {{--<a href="{{url('/admin/programs/create')}}" class="btn btn-primary">--}}
                {{--<i class="fa fa-plus"></i> Добавить--}}
            {{--</a>--}}


        {{--</div>--}}
@stop

@section('content')

    <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Название</th>
            <th>ЭЦД, Ккал</th>
            <th>Длительность, д.</th>
            <th>Число приемов</th>
            <th>Цена, ₽</th>
            <th>Описание</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
        <tr>
            <td>{{$item->name}}</td>
            <td>{{$item->evd}}</td>
            <td>{{$item->duration}}</td>
            <td>{{$item->mpd}}</td>
            <td>{{$item->price}}</td>
            <td>{!! $item->description !!}</td>
            <td>
                <form class="delete" action="{{ route('programs.destroy',$item->id) }}" method="POST">

                    <a href="{{url('admin/programs/'.$item->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger btn-sm" onsubmit="confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                </form>
            </td>
        </tr>
            @endforeach
        </tbody>
    </table>

@stop
</div>

@section('js')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Удалить набор?");
        });

    </script>
@stop