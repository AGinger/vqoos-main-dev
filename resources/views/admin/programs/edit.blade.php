@extends('adminlte::page')

@section('title', 'Наборы')
<div class="panel panel-default box box-primary" id="app">
    @section('content_header')
        <h1>Редактировать программу</h1>
        <div class="panel-heading fa-pull-right">


        </div>
    @stop

    @section('content')

        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->

                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="false">Общее</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Блюда</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-two-tabContent">
                                <div class="tab-pane fade active show" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <!-- form start -->
                                    {!! Form::open(['url' => 'admin/programs/'.$item->id, 'method' => 'put']) !!}

                                        {{csrf_field()}}
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label class="fa-pull-left" for="name">Название</label>
                                               {!! Form::text('name', $item->name, ['class'=>'form-control', 'id'=>'name']) !!}
                                            </div>
                                            <div class="form-group">
                                                <label class="" for="description">Описание</label>
                                                <textarea name="description" class="textarea" id="description" placeholder="Описание">{{$item->description}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="fa-pull-left" for="evd">Энергетическая ценность в ккал</label>
                                                {!! Form::select('evd', [750 => '750', 1000 => '1000',1500=>'1500',2000=>'2000',2500=>'2500',3500=>'3500'],
                                                $item->evd, ['class'=>'form-control', 'id'=>'evd','placeholder'=>'Выберите']) !!}

                                            </div>
                                            <div class="form-group">
                                                <label class="fa-pull-left" for="duration">Продолжительность в днях</label>
                                                {!! Form::select('duration', [2 => '2', 4 => '4',5=>'5',6=>'6',12=>'12',20=>'20',24=>'24',30=>'30'],
                                 $item->duration, ['class'=>'form-control', 'id'=>'duration','placeholder'=>'Выберите', 'disabled'=>true]) !!}
                                            </div>
                                            <div class="form-group">
                                                <label class="fa-pull-left" for="mpd">Число приемов пищи в день</label>
                                                {!! Form::text('mpd', $item->mpd, ['class'=>'form-control', 'id'=>'mpd', 'disabled'=>true]) !!}
                                            </div>

                                            <div class="form-group">
                                                <label class="fa-pull-left" for="price">Цена</label>
                                                {!! Form::text('price', $item->price, ['class'=>'form-control', 'id'=>'price']) !!}
                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                                    <div class="row">

                                    <div class="col-7 col-sm-9">

                                        <!-- The time line -->
                                        <div class="timeline">
                                        @foreach ($program_dishes as $day => $programs_list)
                                            <!-- timeline time label -->
                                            <div class="time-label">
                                                <span class="bg-red">День {{$day}}</span>
                                            </div>
                                            <!-- /.timeline-label -->
                                            @foreach ($programs_list as $program_dish)
                                            <!-- timeline item -->
                                            <div>
                                                <i class="fas fa-utensils bg-blue"></i>
                                                <div class="timeline-item">
                                                    <span>
                                         Прием пищи {{$program_dish->prg_day_meal}}
                                        </span>
                                                    @if($program_dish->dish)
                                                        <h3 class="timeline-header"><a href="{{url('admin/dishes/'.$program_dish->dish_id.'/edit')}}">{{$program_dish->dish->name}}</a></h3>
                                                        <div class="timeline-body row">
                                                            <div class="col-6">
                                                                <img src="{{asset('uploads/dishes'.'/'.$program_dish->dish->photo)}}">
                                                            </div>

                                                            <div class="col-6">
                                                                <a href="#" class="btn btn-danger">
                                                                    <i class="fas fa-trash">
                                                                    </i>
                                                                    Отвязать
                                                                </a>
                                                            </div>

                                                        </div>
                                                    @else
                                                        <div class="timeline-body">
                                                            <h3 class="timeline-header">Блюдо не выбрано</h3>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            @endforeach
                                            <!-- END timeline item -->
                                            @endforeach

                                            <div>
                                                <i class="fas fa-clock bg-gray"></i>
                                            </div>

                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>

            </div>

        </div>

    @stop

    @section('css')
        <link rel="stylesheet" href="{{asset('/vendor/summernote/summernote-bs4.css')}}">
    @stop

    @section('js')
        <script src="{{asset('/vendor/summernote/summernote-bs4.min.js')}}"></script>
        <script src="{{asset('/js/vue.min.js')}}"></script>
        <script src="{{asset('/js/axios.min.js')}}"></script>
        <script>
            var app = new Vue({
                el: '#app',
                data: {
                   selected_dishes:{!! $program_dishes->toJson() !!}
                },
                methods: {
                    storeProgramDishes(day, meal, dish_id) {
                        var self = this;
                        axios.post('/order/goToPrint', {
                            order_id: order.id
                        })
                            .then(function (response) {
                                if (response.data.result === 'success') {
                                    console.log(response);
                                    toastr.success('Заказ принят в работу', '');
                                    // event.target.textContent = response.data.item.designphase.user.name+', '+response.data.item.designphase.created_at;

                                    // Vue.set(self.new_orders, 'activephase', response.data.new_orders);
                                }
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    },
                }
            });

            $('#description').summernote();
        </script>
@stop