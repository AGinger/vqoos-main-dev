@extends('adminlte::page')

@section('title', 'Наборы')
<div class="panel panel-default box box-primary">
    @section('content_header')
        <h1>Добавить программу</h1>
        <div class="panel-heading fa-pull-right">


        </div>
@stop

@section('content')

            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                        <!-- general form elements -->

                        <div class="card card-primary card-tabs">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form role="form" action="{{url('admin/programs')}}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="fa-pull-left" for="name">Название</label>
                                        <input name="name" type="text" class="form-control" id="name" placeholder="Название">
                                    </div>
                                    <div class="form-group">
                                        <label class="" for="description">Описание</label>
                                        <textarea  name="description" class="textarea" id="description" placeholder="Описание"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="fa-pull-left" for="evd">Энергетическая ценность в ккал</label>
                                        <select name="evd" class="form-control" id="evd">
                                            <option value="750">750</option>
                                            <option value="1000">1000</option>
                                            <option value="1500">1500</option>
                                            <option value="2000">2000</option>
                                            <option value="2500">2500</option>
                                            <option value="3500">3500</option>
                                        </select>
                                        {{--<input type="text" class="form-control" id="evd" placeholder="ЭЦД">--}}
                                    </div>
                                    <div class="form-group">
                                        <label class="fa-pull-left" for="exampleInputPassword1">Продолжительность в днях</label>
                                        <select name="duration" class="form-control" id="duration">
                                            <option value="2">2</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="12">12</option>
                                            <option value="20">20</option>
                                            <option value="24">24</option>
                                            <option value="30">30</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="fa-pull-left" for="mpd">Число приемов пищи в день</label>
                                        <input name="mpd" type="text" class="form-control" id="mpd" placeholder="Число приемов">
                                    </div>

                                    <div class="form-group">
                                        <label class="fa-pull-left" for="price">Цена</label>
                                        <input required name="price" type="text" class="form-control" id="price" placeholder="Цена">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </div>
                            </form>
                        </div>


                        </div>

                </div>

            </div>

@stop

        @section('css')

            <link rel="stylesheet" href="{{asset('/vendor/summernote/summernote-bs4.css')}}">
        @stop

        @section('js')
            <script src="{{asset('/vendor/summernote/summernote-bs4.min.js')}}"></script>
            <script> $('#description').summernote(); </script>
        @stop