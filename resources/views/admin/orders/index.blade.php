@extends('adminlte::page')

<div class="panel panel-default box box-primary">
    @section('content_header')
        <h1>Заказы</h1>
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">

            </div>
            <ul class="nav navbar-nav">
                <li> <a href="{{url('/admin/orders/create')}}" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Добавить
                    </a></li>
            </ul>
        </nav>
    @stop

    @section('content')

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Имя</th>
                <th>Оплачен</th>
                <th>Дата доставки</th>
                <th>Период доставки</th>
                <th>Цена</th>
                <th>Промокод</th>
                <th>Статус</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{$item->name}}-{{\Carbon\Carbon::parse($item->created_at)->format('d.m.y')}}</td>
                    <td>{!! $item->payment_status !!}</td>
                    <td>{!! $item->delivery_date !!}</td>
                    <td>{!! $item->delivery_period !!}</td>
                    <td>{!! $item->price !!}</td>
                    <td>{!! $item->promocode !!}</td>
                    <td>{{$item->statuses->first()->status->name}}</td>
                    <td>
                        <form class="delete" action="{{ route('orders.destroy',$item->id) }}" method="POST">

                            <a href="{{url('admin/orders/'.$item->id.'/edit')}}" class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>

                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @stop
</div>

@section('js')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Удалить заказ?");
        });

    </script>
@stop