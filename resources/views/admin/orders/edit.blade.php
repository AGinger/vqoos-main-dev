@extends('adminlte::page')

<div class="panel panel-default box box-primary" id="app">
    @section('content_header')
        <h1>Редактировать заказ</h1>

        <div class="panel-heading">
            <h5>Заказ {{$item->name}} от {{\Carbon\Carbon::parse($item->created_at)->format('d.m.y')}} </h5>
        </div>
    @stop

    @section('content')

        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->

                    <div class="card card-primary card-tabs">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" action="{{url('admin/orders/'.$item->id)}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="fa-pull-left" for="promocode">Программа</label>
                                    <div class="input-group">
                                    <input disabled="" value="{{$item->program->name}}" name="promocode" type="text" class="form-control" id="promocode" placeholder="До 10 символов, без пробелов">
                                    <div class="input-group-append"><a href="{{url('admin/orders/'.$item->program->id.'/edit')}}" class="btn btn-info">Посмотреть</a></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="discount_amount">Цена программы</label>
                                    <input disabled="" value="{{$item->program->price}}" type="text" class="form-control" id="discount_amount">
                                </div>
                                <div class="row">
                                    <div class="col-3 ">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="delivery_count">Количество доставок</label>
                                            <input value="{{$item->delivery_count}}" name="delivery_count" type="text" class="form-control" id="delivery_count"/>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="single_delivery_price">Стоимость одной доставки</label>
                                            <input value="@if($item->delivery_count!=0){{$item->delivery_price/$item->delivery_count}}@else{{$item->delivery_price}}@endif" name="single_delivery_price" type="text" class="form-control" id="single_delivery_price">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="delivery_price">Общая стоимость доставки</label>
                                            <input value="{{$item->delivery_price}}" name="delivery_price" type="text" class="form-control" id="delivery_price">
                                        </div>
                                    </div></div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="address">Адрес доставки</label>
                                    <input value="{{$item->address}}" name="address" type="text" class="form-control" id="address">
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="delivery_period">Период доставки</label>
{{--                                    <input value="{{$item->delivery_period}}" name="delivery_period" type="text" class="form-control" id="delivery_period">--}}
                                    <select name="delivery_period" class="form-control" id="delivery_period">
                                        <option value="9.00-14.00">9.00 - 14.00</option>
                                        <option value="10.00-14.00">10.00 - 14.00</option>
                                        <option value="11.00-14.00">11.00 - 14.00</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-3 ">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="price">Стоимость заказа</label>
                                            <input value="{{$item->program->price+$item->delivery_price}}" name="price" type="text" class="form-control" id="price"/>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="promocode">Промокод</label>
                                            <input disabled="disabled" value="{{$item->promocode}}" name="promocode" type="text" class="form-control" id="promocode">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="discount_amount">Скидка по промокоду в %</label>
                                            <input disabled="disabled" value="@if($item->coupon){{$item->coupon->discount_amount}}@endif" name="discount_amount" type="text" class="form-control" id="discount_amount">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="final_price">Стоимость заказа со скидкой</label>
                                            <input value="{{$item->price}}" name="final_price" type="text" class="form-control" id="final_price">
                                        </div>
                                    </div></div>
                                <div class="row">
                                    <div class="col-4">
                                <div class="form-group">
                                    <label class="fa-pull-left" for="discount_amount">Предоплачено</label>
                                    <input value="" name="discount_amount" type="text" class="form-control" id="discount_amount">
                                </div></div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="fa-pull-left" for="discount_amount">Осталось оплатить</label>
                                            <input value="" name="discount_amount" type="text" class="form-control" id="discount_amount">
                                        </div></div></div>
                                <div class="form-group">
                                    <label class="" for="delivery_note">Примечание заказчика</label>
                                    <textarea name="delivery_note" class="textarea" id="delivery_note">{{$item->delivery_note}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="" for="note">Примечание менеджера</label>
                                    <textarea name="note" class="textarea" id="note">{{$item->note}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label class="">Способ оплаты</label>
                                    <div class="form-check">
                                        <input @if($item->payment_type=='prepay') checked @endif name="payment_type" class="form-check-input" type="radio" value="prepay" >
                                        <label class="form-check-label">Безнал сейчас</label>
                                    </div>
                                    <div class="form-check">
                                        <input @if($item->payment_type=='postpay') checked @endif name="payment_type" class="form-check-input" type="radio"value="postpay"  >
                                        <label class="form-check-label">Курьеру безналом</label>
                                    </div>
                                    <div class="form-check">
                                        <input @if($item->payment_type=='cash') checked @endif name="payment_type" class="form-check-input" type="radio" value="cash" >
                                        <label class="form-check-label">Курьеру налом</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="fa-pull-left" for="status">Статус заказа</label>

                                    <select name="status" class="form-control" id="status">
                                        <option value="3">Не подтвержден</option>
                                        <option value="2">Подтвержден</option>
                                        <option value="4">Доставка</option>
                                        <option value="5">Доставлен</option>
                                        <option value="6">Закрыт</option>
                                    </select>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>


    @stop


        @section('css')

            <link rel="stylesheet" href="{{asset('/vendor/air/css/datepicker.css')}}">
        @stop

        @section('js')

            <script src="{{asset('/vendor/air/js/datepicker.js')}}"></script>
            <script>

                $('#datepicker').datepicker({dateFormat: "yyyy-mm-dd"});

            </script>
@stop