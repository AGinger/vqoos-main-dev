<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Личные данные :: VQOOS</title>
    <link rel="stylesheet" href="{{asset('app/frontend/icons/css/fontello.css')}}">
    <!--[if IE 7]><link rel="stylesheet" href="css/" + font.fontname + "-ie7.css"><![endif]-->
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('css/personal-area.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
<div class="wrapper">
    <div class="left-sidebar">
        <a class="left-sidebar__logo">
            <i class="icon icon-logo-sidebar"></i>
        </a>
        <nav class="left-sidebar__navigation">
            <ul>
                <li><a href="{{url('order')}}" class="{{ request()->is('order*') ? 'active-link' : '' }} ">
                        <i class="icon icon-main"></i>
                        ГЛАВНЫЙ ПУЛЬТ
                    </a></li>
                <li><a href="{{url('profile')}}" class="{{ request()->is('profile') ? 'active-link' : '' }} ">
                        <i class="icon icon-user_outline"></i>
                        ЛИЧНЫЕ ДАННЫЕ
                    </a href></li>
                <li><a href="{{url('orders-history')}}" class="{{ request()->is('orders-history') ? 'active-link' : '' }} ">
                        <i class="icon icon-history"></i>
                        ИСТОРИЯ ЗАКАЗОВ
                    </a></li>
            </ul>
        </nav>
        <div class="left-sidebar__phone">
            Возникли проблемы? <br>+7 925 865-98-98
        </div>
        <div class="left-sidebar__footer">
            Nora 1.0
        </div>
    </div>
@yield('content')
</div>
<script src="{{asset('js/vue.min.js')}}"></script>
<script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{asset('js/vue-clipboard.min.js')}}"></script>

@yield('js')
</body>
</html>