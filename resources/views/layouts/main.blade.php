<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VQOOS</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{asset('css/flickity.min.css')}}">
    <!-- <link rel="stylesheet" href="icons/css/animation.css"> -->
    <link rel="stylesheet" href="{{asset('app/frontend/icons/css/fontello.css')}}">
    <!-- [if IE 7]><link rel="stylesheet" href="css/" + font.fontname + "-ie7.css"><![endif]-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/reset.css')}}">
</head>

<body>
<div id="main-wrapper" class="wrapper">

    <header class="header">
        <div class="header__overlay">
            <a href="{{url('/')}}" class="header__logo"></a>
            <button id="toggle-mobile-menu" class="header__hamburger">
                <i class="icon">&#xe822;</i>
            </button>
            <nav class="header__top-nav">
                <ul>
                    <li><a href="#payment">Оплата</a></li>
                    <li><a href="#delivery">Доставка</a></li>
                    <li><a href="#faq">ЧаВо</a></li>
                    <ul class="header__mobile-links">
                        <li><a href="#">О компании</a></li>
                        <li><a href="#">Политика ОПД</a></li>
                        <li><a href="#">Условия </a></li>
                        <li><a href="#">Оферта</a></li>
                    </ul>
                </ul>
            </nav>
        </div>

        <div class="header__overlay header__right-block">
            @auth
            <a class="header__right-block-phone" href="{{url('/profile')}}">+7 (925) 922-99-66</a>
                <a class="logout" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="icon icon-exit"></i>
                    выйти
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endauth
            @guest
            <a href="{{url('/register')}}"><i class="icon">&#xe80c;</i>регистрация</a>
            <a href="{{url('/login')}}"><i class="icon">&#xe80d;</i>вход</a>
            @endguest

        </div>
    </header>

@yield('content')

    <div class="delivery" id="delivery">
        <h2>ДОСТАВКА</h2>
        <p>Доставка пн-вт-чт или пн-ср-пт в любой часовой интервал с 6 до 12 утра.</p>
        <div class="color-scheme">
            <div class="green"></div>
            <span>0 ₽</span>
            <div class="orange"></div>
            <span>200 ₽</span>
        </div>
        <div class="map" style="position:relative;overflow:hidden;">
            <a href="https://yandex.ru/maps?utm_medium=mapframe&utm_source=maps"
               style="color:#eee;font-size:12px;position:absolute;top:0px;">Яндекс.Карты</a>
            <a href="https://yandex.ru/maps/?ll=37.659120%2C55.738598&mode=usermaps&source=constructorLink&um=constructor%3A7f408aa558f75844038c986a614a4a3cc0c5ae1197a0445b8b1458ecd605edfd&utm_medium=mapframe&utm_source=maps&z=8.8"
               style="color:#eee;font-size:12px;position:absolute;top:14px;">Яндекс.Карты — поиск мест и адресов,
                городской транспорт</a>
            <iframe src="https://yandex.ru/map-widget/v1/-/CKqvIRIi" height="528" frameborder="0"
                    allowfullscreen="true"></iframe>
        </div>
    </div>

    <div class="payment" id="payment">
        <h2>ОПЛАТА</h2>
        <p>Оплатить заказ можно на сайте или при получении. При оплате на сайте вы получите скидку на доставку.
            Чтобы получить оплаченный заказ, вам понадобится документ, удостоверяющий личность. Способ оплаты может
            быть не доступен для выбранного способа доставки или региона.</p>
        <h3>НАЛИЧНЫЕ</h3>
        <p>Чтобы оплатить заказ наличными при получении, при оформлении заказа выберите «Оплатить наличными» и
            подтвердите заказ. После оплаты заказа вы получите чек на указанную электронную почту.</p>
        <h3>ОПЛАТА КАРТОЙ</h3>
        <p>При оформлении заказа выберите «Онлайн банковской картой» и нажмите «Перейти к оплате онлайн».<br>
            Введите данные карты и нажмите «Оплатить». Минимальная сумма оплаты — 1 рубль.
            <br><br>К оплате принимаются банковские карты, у которых 16, 18, 19 цифр в номере: <br>VISA, MasterCard,
            American Express;<br> VISA Electron/Plus, Cirrus/Maestro, если у них есть код CVC2 и CVV2; <br>МИР.
            <br>После оплаты заказа вам на почту придет электронный чек и подробности заказа.</p>
        <div class="partners">
            <img src="{{asset('images/brands.png')}}" alt="Партнеры">
            <!-- <img src="images/mastercard.svg" alt="mastercard">
            <img src="images/visa.svg" alt="visa">
            <img src="images/tinkoff.svg" alt="tinkoff">
            <img src="images/mir.svg" alt="mir">
            <img src="images/alfa-bank.svg" alt="alfa-bank">
            <img src="images/yandex.svg" alt="yandex">
            <img src="images/jcb.svg" alt="jcb">
            <img src="images/wechat.svg" alt="wechat">
            <img src="images/qiwi.svg" alt="qiwi">
            <img src="images/wm.svg" alt="wm">
            <img src="images/logo1.svg" alt="logo1"> -->
        </div>
    </div>

    <div class="faq" id="faq">
        <h2>Частые вопросы</h2>
        <div class="faq__spoilers">
            <div class="spoiler">
                <header class="spoiler__header">
                    <span>Как разрабатывается программа питания?</span>
                    <i class="icon">&#xe82a;</i>
                </header>
                <div class="spoiler__body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, maiores quis? Odit, officia
                    aut sint quia quisquam pariatur ipsam ut!
                </div>
            </div>
            <div class="spoiler">
                <header class="spoiler__header">
                    <span>Могу ли я быть уверен в качестве продукции?</span>
                    <i class="icon">&#xe82a;</i>
                </header>
                <div class="spoiler__body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, maiores quis? Odit, officia
                    aut sint quia quisquam pariatur ipsam ut!
                </div>
            </div>
            <div class="spoiler">
                <header class="spoiler__header">
                    <span>Что входит в стоимость?</span>
                    <i class="icon">&#xe82a;</i>
                </header>
                <div class="spoiler__body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, maiores quis? Odit, officia
                    aut sint quia quisquam pariatur ipsam ut!
                </div>
            </div>
            <div class="spoiler">
                <header class="spoiler__header">
                    <span>Успеет ли ко мне курьер в такой короткий интервал?</span>
                    <i class="icon">&#xe82a;</i>
                </header>
                <div class="spoiler__body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, maiores quis? Odit, officia
                    aut sint quia quisquam pariatur ipsam ut!
                </div>
            </div>
            <div class="spoiler">
                <header class="spoiler__header">
                    <span>Могу ли я приостановить доставку?</span>
                    <i class="icon">&#xe82a;</i>
                </header>
                <div class="spoiler__body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, maiores quis? Odit, officia
                    aut sint quia quisquam pariatur ipsam ut!
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="footer__top">
            <a href="index.html" class="header__logo"></a>
            <div>
                <ul>
                    <li><a href="privacy-policy.html">Политика конфиденциальности</a></li>
                    <li><a>Условия использования</a></li>
                    <li><a>Оферта</a></li>
                </ul>
            </div>
            <div>
                <ul>
                    <li><a>Программы</a></li>
                    <li><a>Оплата</a></li>
                    <li><a>Доставка</a></li>
                    <li><a>Частые вопросы</a></li>
                    <li><a>О компании</a></li>
                </ul>
            </div>
            <div class="footer__get-in-touch">
                <a href="tel:+79259229966" class="footer__phone">+7 (925) 922-99-66</a>
                <div class="footer__soc">
                    <a class="facebook"></a>
                    <a class="vk"></a>
                    <a class="instagram"></a>
                </div>
                <a href="mailto:info@vqoos.com" class="footer__email">info@vqoos.com</a>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="footer__copyright">© 2020 Все права принадлежат ИП «Иванов Иван» ОГРН: 1187746870497 ИНН:
                7724457620</div>
            <span>сделано WLD в 2020 году</span>
        </div>
    </footer>
</div>


<script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('js/flickity.min.js')}}"></script>
<script src="{{asset('js/vue.min.js')}}"></script>
<script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{asset('js/vue-clipboard.min.js')}}"></script>

@yield('js')
<script src="{{asset('js/common.js')}}"></script>
</body>

</html>
